
import csv
import geocoder

outfile = open ("souradnice.csv", 'w')
outfile_header = "Rovnobezka,Polednik,Mesto,Oblast,Stat\n"
outfile.write(outfile_header)
latitude = 0.00
longitude = 0.00
with open ("coordinates.csv", 'r') as infile:
    reader = csv.reader(infile)
    header = next(reader)
    for row in reader:
        if row[0] == 'latitude':
            latitude = row[1]
        else:
            longitude = row[1]
            g = geocoder.osm([latitude,longitude],method = 'reverse')
            line = "{},{},{},{},{}\n".format(latitude,longitude,g.city,g.state,g.country)
            outfile.write(line)
outfile.close()